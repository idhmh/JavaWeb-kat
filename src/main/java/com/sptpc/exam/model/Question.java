package com.sptpc.exam.model;

/**
 * Question
 * Description:
 * date: 2019/9/29 8:42 下午
 *
 * @author alexchen
 */
public class Question {
    private String id;
    private String question;
    private String[] options;
    private String answer;

    public Question() {
    }

    public Question(String id, String question, String[] options, String answer) {
        this.id = id;
        this.question = question;
        this.options = options;
        this.answer = answer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
