package com.sptpc.exam.model;

public class User {
    private String id;
    private String name;
    private String college;
    private String classname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public User(String id, String name, String college, String classname) {
        this.id = id;
        this.name = name;
        this.college = college;
        this.classname = classname;
    }
}