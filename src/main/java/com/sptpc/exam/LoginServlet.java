package com.sptpc.exam;

import com.sptpc.exam.model.DBHelper;
import com.sptpc.exam.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 检查验证码
        String code = request.getParameter("verify");
        // 生成验证码后，kaptcha会把验证码写入session中以KAPTCHA_SESSION_KEY为名字保存
        String verifyCode = (String) request.getSession().getAttribute("KAPTCHA_SESSION_KEY");
        if (code!=null &&code.equals(verifyCode)) {
            // 1.1 成功
            // 2. 检查用户名和密码
            String id = request.getParameter("id");
            request.getSession().setAttribute("id",id);
            List<User> users = new ArrayList<>();

            String password = request.getParameter("password");

            Connection connection = DBHelper.getConnection();
            String sql = "SELECT password from user where uid = ?";
            String sql2 = "SELECT 学院, 班级, 姓名, 余额 from student where 学号 = ?";
            try {

                PreparedStatement pstmt = connection.prepareStatement(sql);
                pstmt.setString(1, id);
                ResultSet rs = pstmt.executeQuery();

                if (rs.next()){
                    String upassword = rs.getString(1);
                    if (password.equals(upassword)){
                        PreparedStatement pstmt2 = connection.prepareStatement(sql2);
                        pstmt2.setString(1, id);
                        ResultSet rs2 = pstmt2.executeQuery();
                        if (rs2.next()){
                            String college = rs2.getString(1);
                            String classname = rs2.getString(2);
                            String uname = rs2.getString(3);
                            int balance = rs2.getInt(4);
                            User user = new User(id,uname,college,classname);
                            user.setCollege(college);
                            user.setName(uname);

                            request.getSession().setAttribute("college",college);
                            request.getSession().setAttribute("classname",classname);
                            request.getSession().setAttribute("username",uname);
                            request.getSession().setAttribute("balance",balance);
                            request.getRequestDispatcher("success.jsp").forward(request,response);
                        }
                    } else {
                        request.setAttribute("errorMsg", "密码错误！");
                        request.getRequestDispatcher("index.jsp").forward(request,response);
                    }
                } else {
                    request.setAttribute("errorMsg", "用户名不存在！");
                    request.getRequestDispatcher("index.jsp").forward(request,response);
                }


            } catch (SQLException e){
                e.printStackTrace();
            }

        } else { // 1.2 返回登录，并给出错误信息
            request.setAttribute("errorMsg", "验证码错误");
            request.getRequestDispatcher("index.jsp").forward(request,response);

        }
    }
}
