package com.sptpc.exam;

import com.sptpc.exam.model.Question;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/initexam.do")
public class InitExamServlet extends HttpServlet {
    // 保存所有的试题
    private final List<Question> questions = new ArrayList<>();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // 加载试题
        String[] options1 = {"成都", "北京", "上海", "广州"};
        Question q1 = new Question("p1","中国的首都在哪里？",
                options1, "2");
        String[] options2 = {"成都", "北京", "上海", "广州"};
        Question q2 = new Question("p2","四川的省会在哪里？",
                options2, "1");
        String[] options3 = {"成都", "德阳", "绵阳", "广元"};
        Question q3 = new Question("p3","四川的第二大城市在哪里？",
                options3, "3");
        questions.add(q1);
        questions.add(q2);
        questions.add(q3);


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: 将加载的试题保存并给试题页面，这里放request里面行不行？
        request.getSession().setAttribute("questions", questions);
        request.getRequestDispatcher("exam.jsp").forward(request, response);
    }
}
