package com.sptpc.exam;

import com.mysql.cj.xdevapi.Result;
import com.sptpc.exam.model.DBHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/recharge.do")
public class RechargeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String addnumber = request.getParameter("addnumber");
        String id = (String) request.getSession().getAttribute("id");


        Connection connection = DBHelper.getConnection();
        String sql = "SELECT 余额 FROM student WHERE 学号 = ?; ";
        String sql2 = "UPDATE student SET `余额` = `余额`+ ? WHERE `学号` = ?;";

        try{

            PreparedStatement pstmt2 = connection.prepareStatement(sql2);
            pstmt2.setString(1,addnumber);
            pstmt2.setString(2,id);
            int res = pstmt2.executeUpdate();
            if (res>0){
                PreparedStatement pstmt = connection.prepareStatement(sql);
                pstmt.setString(1, id);
                ResultSet rs = pstmt.executeQuery();
                if(rs.next()){
                    String balance = rs.getString(1);
                    request.getSession().setAttribute("balance",balance);
                    request.getRequestDispatcher("recharge.jsp").forward(request,response);
                }
            }else{
                System.out.println("更新失败");
            }


        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
