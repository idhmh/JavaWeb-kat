<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>水卡线上余额查询系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.4.0/jquery.min.js"></script>
</head>
<style>
    *{padding: 0;margin: 0;}
    .top{border-bottom: black 1px solid;height: 70px}
    .top h2{float: left;padding-left: 30px;line-height: 70px;}
    .top_box{float: right;color: black;padding-right: 30px;}
    .top_box{line-height: 70px;}
    .center{width: 100%;text-align: center;margin-top: 100px;}
    .buttonc{border-radius: 40%;}
</style>
<body>
<div class="top">
    <h2>水卡线上余额查询系统</h2>
    <div class="top_box">
        <c:choose>
            <c:when test="${sessionScope.username != null}">
                ${sessionScope.username}(<a href="logout.do" class="top_a">登出</a>)
            </c:when>
            <c:otherwise>
                <a href="index.jsp" class="top_a">请登录</a>
            </c:otherwise>
        </c:choose>
    </div>

</div>