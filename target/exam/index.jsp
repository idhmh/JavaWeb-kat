<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>水卡线上余额查询系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.4.0/jquery.min.js"></script>
</head>
<style>
    title{color: #1a1873}
    .center{
        text-align: center;
        margin-top: 50px;
    }
    .pwd{
        margin-left: 16px;
    }
    body{
        background-color: #bdffee;
    }
    .buttonc{
        border-radius: 40%;
    }
    h1{
        font-size: 70px;
        color: #1a1873;
        font-weight: bolder;
        font-style: initial;
    }
</style>
<body>

<div class="center">

    <h1>水卡线上余额查询</h1>
    <br><br><br>
    <form method="get" action="login.do">
        用户名：<input type="text" name="id"><br><br>
        密码：<input type="password" name="password" class="pwd"><br><br>
        验证码：<input type="text" name="verify"><br><br>
        <img id="verification" alt="验证码" src="/kaptcha.jpg" style="cursor:pointer;" title="看不清？换一张"/>
        <%
            if (request.getAttribute("errorMsg") != null) {
        %>
        <div style="color: red"><%=request.getAttribute("errorMsg")%></div>
        <%
            }
        %>
        <br><br>
        <input type="submit" value="登录" class="buttonc">
        <input type="reset" value="重置" class="buttonc">
        <a href="/register.do">注册</a>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        // 刷新验证码
        $("#verification").bind("click", function(){
            $(this).hide().attr('src', '/kaptcha.jpg?random=' + Math.random()).fadeIn();
        });
    });
</script>
</body>
</html>
